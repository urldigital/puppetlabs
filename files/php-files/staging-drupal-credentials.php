<?php

    const DRUPAL_CREDENTIALS = [
        'beyond79.com' => [
            'database' => ['host' => 'staging-drupal.cs4ybiuq7yk8.us-west-2.rds.amazonaws.com', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_beyond79'],
            'memcached' => ['host' => 'staging.nfscache.beyond79.com', 'port' => '11211'],
        ],
        'elitediamondbuyers.com' => [
            'database' => ['host' => 'staging-drupal.cs4ybiuq7yk8.us-west-2.rds.amazonaws.com', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_edb'],
            'memcached' => ['host' => 'staging.nfscache.beyond79.com', 'port' => '11211'],
        ],
        'goldkit.com' => [
            'database' => ['host' => 'staging-drupal.cs4ybiuq7yk8.us-west-2.rds.amazonaws.com', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_gk'],
            'memcached' => ['host' => 'staging.nfscache.beyond79.com', 'port' => '11211'],
        ],
        'outofyourlife.com' => [
            'database' => ['host' => 'staging-drupal.cs4ybiuq7yk8.us-west-2.rds.amazonaws.com', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_ooyl'],
            'memcached' => ['host' => 'staging.nfscache.beyond79.com', 'port' => '11211'],
        ],
        'recycleplatinum.com' => [
            'database' => ['host' => 'staging-drupal.cs4ybiuq7yk8.us-west-2.rds.amazonaws.com', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_rp'],
            'memcached' => ['host' => 'staging.nfscache.beyond79.com', 'port' => '11211'],
        ],
        'sellyourgold.com' => [
            'database' => ['host' => 'staging-drupal.cs4ybiuq7yk8.us-west-2.rds.amazonaws.com', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_syg'],
            'memcached' => ['host' => 'staging.nfscache.beyond79.com', 'port' => '11211'],
        ],
        'thingswebuy.com' => [
            'database' => ['host' => 'staging-drupal.cs4ybiuq7yk8.us-west-2.rds.amazonaws.com', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_twb'],
            'memcached' => ['host' => 'staging.nfscache.beyond79.com', 'port' => '11211'],
        ],
    ];