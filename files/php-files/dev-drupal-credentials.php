<?php

    const DRUPAL_CREDENTIALS = [
        'beyond79.com' => [
            'database' => ['host' => 'localhost', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_beyond79'],
            'memcached' => ['host' => 'localhost', 'port' => '11211'],
        ],
        'elitediamondbuyers.com' => [
            'database' => ['host' => 'localhost', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_edb'],
            'memcached' => ['host' => 'localhost', 'port' => '11211'],
        ],
        'goldkit.com' => [
            'database' => ['host' => 'localhost', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_gk'],
            'memcached' => ['host' => 'localhost', 'port' => '11211'],
        ],
        'outofyourlife.com' => [
            'database' => ['host' => 'localhost', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_ooyl'],
            'memcached' => ['host' => 'localhost', 'port' => '11211'],
        ],
        'recycleplatinum.com' => [
            'database' => ['host' => 'localhost', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_rp'],
            'memcached' => ['host' => 'localhost', 'port' => '11211'],
        ],
        'sellyourgold.com' => [
            'database' => ['host' => 'localhost', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_syg'],
            'memcached' => ['host' => 'localhost', 'port' => '11211'],
        ],
        'thingswebuy.com' => [
            'database' => ['host' => 'localhost', 'user' => 'd7', 'password' => 'drupal', 'database' => 'd7_twb'],
            'memcached' => ['host' => 'localhost', 'port' => '11211'],
        ],
    ];